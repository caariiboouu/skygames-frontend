const app = new Vue({
  el: "#app",
  data: {
    isActive: false,
    hasError: false,
    message: "",
    logs: [],
    status: "disconnected"
  },
  methods: {
    connect() {
      this.socket = new WebSocket("wss://echo.websocket.org");
      this.socket.onopen = () => {
        this.status = "connected";
        this.isActive = true;
        this.logs.push({ event: "Connected to", data: 'wss://echo.websocket.org'})


        this.socket.onmessage = ({data}) => {
          this.logs.push({ event: "Recieved message", data });
        };
      };
    },
    disconnect() {
      this.socket.close();
      this.status = "disconnected";
      this.isActive = false;
      this.logs = [];
    },
    sendMessage(e) {
      this.socket.send(this.message);
      this.logs.push({ event: "Sent message", data: this.message });
      this.message = "";
    }
  }
});
